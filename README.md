# Timer
An Android timer app written in Kotlin.

## Functionalities & features:
- Set the timer for 1-12 seconds (inclusive) with a drag gesture
- Start the timer
- Cancel the timer
- Cancelled timer resets
- Background animates to different colors when the timer is running
- System UI bars transparency
- There is a flash when the timer finishes
- Timer is not interactive during countdown
- The time set is indicated also during countdown
- You can only increase / decrease the timer value by 1 whole second
- You cannot increase the timer value to a value above 12 seconds
- Timer resets when app is sent to background

![Timer screen 1](img/screen1.jpg)
![Timer screen 2](img/screen2.jpg)
