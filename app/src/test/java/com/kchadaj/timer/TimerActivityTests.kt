package com.kchadaj.timer

import android.graphics.Color
import android.support.design.widget.FloatingActionButton
import kotlinx.android.synthetic.main.activity_timer.*
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner

import org.mockito.Mockito.verify

@RunWith(RobolectricTestRunner::class)
class TimerActivityTests {

    @Mock
    private lateinit var timerPresenter: TimerPresenter

    private lateinit var timerActivity: TimerActivity

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        timerActivity = Robolectric.buildActivity(TimerActivity::class.java).create().get()
        timerActivity.timerPresenter = timerPresenter
    }

    @Test
    fun onTimerButtonClick_shouldAskPresenter() {
        val timerButton = timerActivity.findViewById<FloatingActionButton>(R.id.fab)
        timerButton.performClick()

        verify(timerPresenter).onTimerButtonClick()
    }

    @Test
    fun updateProgress_shouldWork() {
        val progress = timerActivity.circularSeekBar.progress
        val expected = progress + 1f
        timerActivity.updateProgress(progress + 1f)
        val actual = timerActivity.circularSeekBar.progress
        assertEquals(expected, actual)
    }

    @Test
    fun enableCircularSeekBar_shouldWork() {
        val isEnabled = timerActivity.circularSeekBar.isEnabled
        val expected = !isEnabled
        timerActivity.enableCircularSeekBar(!isEnabled)
        val actual = timerActivity.circularSeekBar.isEnabled
        assertEquals(expected, actual)
    }

    @Test
    fun setTimerTextView_shouldChangeText() {
        val expected = "11:22"
        timerActivity.setTimerTextView(expected)
        val actual = timerActivity.timerTextView.text
        assertEquals(expected, actual)
    }
}
