package com.kchadaj.timer

import android.os.CountDownTimer

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner

import org.mockito.Mockito.verify

@RunWith(RobolectricTestRunner::class)
class TimerPresenterTests {

    @Mock
    private lateinit var view: TimerViewContract

    @Mock
    private lateinit var timer: CountDownTimer

    private lateinit var timerPresenter: TimerPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        timerPresenter = TimerPresenter(view)
        timerPresenter.timer = timer
    }

    @Test
    fun onViewPause_shouldStopTimerAndRevertView() {
        timerPresenter.onViewPause()

        verify(timer).cancel()
        verify(view).revertView()
    }

    @Test
    fun onTimerButtonFirstClick_shouldStartTimerAndSetViewActions() {
        timerPresenter.onTimerButtonClick()

        verify(view).runColorTransitionAnimations()
        verify(view).setTimerButtonDrawable(R.drawable.ic_timer_off)
    }

    @Test
    fun onTimerButtonSecondClick_shouldStopTimerAndRevertView() {
        timerPresenter.onTimerButtonClick()
        timerPresenter.timer = timer
        timerPresenter.onTimerButtonClick()

        verify(timer).cancel()
        verify(view).revertView()
    }

    @Test
    fun onProgressIndexChanged_shouldAskViewToUpdateTextViewAndProgress() {
        timerPresenter.onProgressIndexChanged(4)

        verify(view).setTimerTextView("04:00")
        verify(view).updateProgress(4f)
    }
}
