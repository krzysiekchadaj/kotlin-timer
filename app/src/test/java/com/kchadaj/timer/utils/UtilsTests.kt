package com.kchadaj.timer.utils

import org.junit.Test

import org.junit.Assert.*

class UtilsTests {

    @Test
    fun formatTime_isCorrect() {
        assertEquals("10:46", Utils.formatTime(10.46f))
        assertEquals("05:46", Utils.formatTime(5.46f))
        assertEquals("05:46", Utils.formatTime(5.45999999f))
        assertEquals("05:46", Utils.formatTime(5.4611111f))
        assertEquals("03:40", Utils.formatTime(3.4f))
        assertEquals("03:00", Utils.formatTime(3.0f))
        assertEquals("01:00", Utils.formatTime(1f))
        assertEquals("00:00", Utils.formatTime(0.00001f))
    }
}
