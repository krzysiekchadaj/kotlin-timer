@file:JvmMultifileClass

import android.content.res.Resources

fun Resources.getDimen(dimenName: String): Int {
    val id = getIdentifier(dimenName, "dimen", "android")
    return when {
        id > 0 -> getDimensionPixelSize(id)
        else -> 0
    }
}

fun Resources.isSoftwareNavBarVisible(): Boolean {
    val id = this.getIdentifier("config_showNavigationBar", "bool", "android")
    return id > 0 && this.getBoolean(id)
}
