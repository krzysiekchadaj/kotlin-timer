@file:JvmMultifileClass

import android.view.View

fun View.addPadding(
        left: Int = this.paddingLeft,
        top: Int = this.paddingTop,
        right: Int = this.paddingRight,
        bottom: Int = this.paddingBottom) {
    setPadding(left, top, right, bottom)
}
