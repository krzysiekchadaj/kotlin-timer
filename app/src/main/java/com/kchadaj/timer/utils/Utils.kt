package com.kchadaj.timer.utils

import java.util.*

class Utils {
    companion object {
        fun formatTime(seconds: Float) =
                String.format(Locale.US, "%05.2f", seconds).replace(".", ":")
    }
}
