package com.kchadaj.timer

interface TimerPresenterContract {

    fun onViewPause()
    fun onTimerButtonClick()
    fun onProgressIndexChanged(progressIndex: Int)
}
