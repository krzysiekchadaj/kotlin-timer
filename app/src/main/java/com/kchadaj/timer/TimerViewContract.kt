package com.kchadaj.timer

interface TimerViewContract {

    fun getProgressIndex(): Int
    fun updateProgress(progress: Float)

    fun enableCircularSeekBar(enable: Boolean)

    fun setTimerButtonDrawable(id: Int)
    fun setTimerTextView(time: String)

    fun runColorTransitionAnimations()
    fun runFlashAnimation()
    fun revertView()
}
