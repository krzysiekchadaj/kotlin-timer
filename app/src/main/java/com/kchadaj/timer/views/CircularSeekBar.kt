package com.kchadaj.timer.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

import com.kchadaj.timer.R

class CircularSeekBar(context: Context, attrs: AttributeSet) : View(context, attrs) {

    var progress: Float = 0f
    var maxProgress: Int = 0
    var rotationAngle: Float = 0f
    var innerRadius: Float = 0f
    var thumbWidth: Float = 0f
    var thumbOffset: Float = 0f
    var labelWidth: Float = 0f
    var stepsOffset: Float = 0f
    var innerColor: Int = 0
    var seekBarColor: Int = 0
    var sliderColor: Int = 0
    var sliderTraceColor: Int = 0
    var labelColor: Int = 0

    var progressIndex: Int = 0
        private set

    private var progressIndexOld: Int = -1
    private var isThumbTouched: Boolean = false
    private var centerX: Float = 0f
    private var centerY: Float = 0f
    private var radius: Float = 0f
    private var thumbAngle: Float = 0f
    private var stepsAngle: Float = 0f
    private val boundingBox = RectF()
    private var paint: Paint = Paint()

    interface OnProgressChangeListener {
        fun onProgressIndexChanged(progressIndex: Int)
    }

    private var onProgressChangeListener: OnProgressChangeListener? = null

    fun setOnProgressChangeListener(onProgressChangeListener: OnProgressChangeListener) {
        this.onProgressChangeListener = onProgressChangeListener
    }

    init {
        initAttributes(context, attrs)

        progressIndex = progress.toInt()
        progressIndexOld = progressIndex

        if (maxProgress < 2) {
            maxProgress = 2
        }

        if (progressIndex >= maxProgress) {
            progressIndex = maxProgress - 1
        }

        stepsAngle = 360f / maxProgress
        thumbAngle = progressIndex * stepsAngle

        initPaint()
    }

    private fun initAttributes(context: Context, attrs: AttributeSet) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.CircularSeekBar, 0, 0)

        progress = a.getInt(R.styleable.CircularSeekBar_progress, 2).toFloat()
        maxProgress = a.getInteger(R.styleable.CircularSeekBar_maxProgress, 12)
        rotationAngle = a.getFloat(R.styleable.CircularSeekBar_rotationAngle, 270f)
        innerRadius = a.getDimension(R.styleable.CircularSeekBar_innerRadius, 135f)
        thumbWidth = a.getDimension(R.styleable.CircularSeekBar_thumbWidth, 10f)
        thumbOffset = a.getDimension(R.styleable.CircularSeekBar_thumbOffset, 4f)
        labelWidth = a.getDimension(R.styleable.CircularSeekBar_labelWidth, 4f)
        stepsOffset = a.getDimension(R.styleable.CircularSeekBar_stepsOffset, 3f)
        innerColor = a.getColor(R.styleable.CircularSeekBar_innerColor, Color.GREEN)
        seekBarColor = a.getColor(R.styleable.CircularSeekBar_seekBarColor, Color.YELLOW)
        sliderColor = a.getColor(R.styleable.CircularSeekBar_sliderColor, Color.MAGENTA)
        sliderTraceColor = a.getColor(R.styleable.CircularSeekBar_sliderTraceColor, Color.RED)
        labelColor = a.getColor(R.styleable.CircularSeekBar_labelColor, Color.CYAN)

        a.recycle()
    }

    private fun initPaint() {
        paint.strokeCap = Paint.Cap.BUTT
        paint.isAntiAlias = true
        paint.isFilterBitmap = true
        paint.isDither = true
        paint.style = Paint.Style.FILL
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.rotate(rotationAngle, centerX, centerY)

        drawSeekBarBackground(canvas)

        if (isEnabled) {
            drawSeekBar(canvas, sliderColor)
        } else {
            drawSeekBar(canvas, sliderTraceColor)

            val actualAngle = progress * stepsAngle
            drawSeekBar(canvas, sliderColor, actualAngle)
        }

        drawInnerCircle(canvas)

        if (isEnabled) {
            drawThumb(canvas, thumbAngle)
        }

        drawSteps(canvas)
    }

    private fun drawSeekBarBackground(canvas: Canvas) {
        paint.color = seekBarColor
        canvas.drawCircle(centerX, centerY, radius, paint)
    }

    private fun drawSeekBar(canvas: Canvas, color: Int, angle: Float = thumbAngle) {
        paint.color = color
        canvas.drawArc(
                centerX - radius,
                centerY - radius,
                centerX + radius,
                centerY + radius,
                0f,
                if (angle <= 0) angle + 360 else angle,
                true,
                paint)
    }

    private fun drawInnerCircle(canvas: Canvas) {
        paint.color = innerColor
        canvas.drawCircle(centerX, centerY, innerRadius, paint)
    }

    private fun drawThumb(canvas: Canvas, angle: Float) {
        val cosThumbAngle = Math.cos(Math.toRadians(angle.toDouble())).toFloat()
        val sinThumbAngle = Math.sin(Math.toRadians(angle.toDouble())).toFloat()

        paint.color = sliderColor
        paint.strokeWidth = thumbWidth + labelWidth

        canvas.drawLine(
                centerX + (innerRadius - thumbOffset) * cosThumbAngle,
                centerY + (innerRadius - thumbOffset) * sinThumbAngle,
                centerX + (radius + thumbOffset) * cosThumbAngle,
                centerY + (radius + thumbOffset) * sinThumbAngle,
                paint)
    }

    private fun drawSteps(canvas: Canvas) {
        paint.color = labelColor
        paint.strokeWidth = labelWidth

        for (i in 0 until maxProgress) {
            val angRad = Math.toRadians((i * stepsAngle).toDouble()).toFloat()

            val cosAngRad = Math.cos(angRad.toDouble()).toFloat()
            val sinAngRad = Math.sin(angRad.toDouble()).toFloat()

            val startX = centerX + (innerRadius + stepsOffset) * cosAngRad
            val startY = centerY + (innerRadius + stepsOffset) * sinAngRad
            val stopX = centerX + (radius - stepsOffset) * cosAngRad
            val stopY = centerY + (radius - stepsOffset) * sinAngRad
            canvas.drawLine(startX, startY, stopX, stopY, paint)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!isEnabled) {
            return super.onTouchEvent(event)
        }

        val cosRotationAngle = Math.cos(Math.toRadians((-rotationAngle).toDouble())).toFloat()
        val sinRotationAngle = Math.sin(Math.toRadians((-rotationAngle).toDouble())).toFloat()

        val x1 = event.x - centerX
        val y1 = event.y - centerY

        val x = centerX + x1 * cosRotationAngle - y1 * sinRotationAngle
        val y = centerY + x1 * sinRotationAngle + y1 * cosRotationAngle

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                val currentThumbAngle = getNearestStepAngle(x, y)
                val currentProgressIndex = (currentThumbAngle / stepsAngle).toInt()
                if (currentProgressIndex == progressIndex || progressIndex == maxProgress && currentProgressIndex == 0) {
                    isThumbTouched = true
                }
            }

            MotionEvent.ACTION_MOVE -> if (isThumbTouched) {
                moveThumb(x, y)
            }

            MotionEvent.ACTION_UP -> if (isThumbTouched) {
                moveThumb(x, y)
                isThumbTouched = false
            }

            else -> return super.onTouchEvent(event)
        }

        invalidate()
        return true
    }

    private fun getNearestStepAngle(x: Float, y: Float): Float {
        val angle = Math.toDegrees(Math.atan2((y - centerY).toDouble(), (x - centerX).toDouble())).toFloat()

        val angleCeil = Math.ceil((angle / stepsAngle).toDouble()).toInt() * stepsAngle
        val angleFloor = Math.floor((angle / stepsAngle).toDouble()).toInt() * stepsAngle

        val nearestStepAngle: Float
        nearestStepAngle = if (Math.abs(angle - angleCeil) < Math.abs(angle - angleFloor)) {
            angleCeil
        } else {
            angleFloor
        }
        return if (nearestStepAngle < 0) nearestStepAngle + 360 else nearestStepAngle
    }

    private fun moveThumb(x: Float, y: Float) {
        if (isThumbTouched) {
            val currentThumbAngle = getNearestStepAngle(x, y)
            val currentProgressIndex = (currentThumbAngle / stepsAngle).toInt()

            if (currentProgressIndex - progressIndexOld != 0
                    && !isTryingToGoBelowMin(currentProgressIndex, progressIndexOld)
                    && !isTryingToGoAboveMax(currentProgressIndex, progressIndexOld)) {

                thumbAngle = currentThumbAngle
                progressIndex = currentProgressIndex
                progressIndexOld = progressIndex

                if (onProgressChangeListener != null) {
                    if (progressIndex == 0) progressIndex = maxProgress
                    onProgressChangeListener!!.onProgressIndexChanged(progressIndex)
                }
            }
        }
    }

    private fun isTryingToGoBelowMin(current: Int, previous: Int): Boolean {
        return previous == 1 && current != 2
    }

    private fun isTryingToGoAboveMax(current: Int, previous: Int): Boolean {
        return previous == 0 && current != maxProgress - 1
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        if (w < h) {
            radius = (w / 2).toFloat()
            centerX = radius
            centerY = (h / 2).toFloat()
        } else {
            radius = (h / 2).toFloat()
            centerX = (w / 2).toFloat()
            centerY = radius
        }

        boundingBox.set(
                centerX - radius,
                centerY - radius,
                centerX + radius,
                centerY + radius
        )

        radius -= thumbOffset
    }
}
