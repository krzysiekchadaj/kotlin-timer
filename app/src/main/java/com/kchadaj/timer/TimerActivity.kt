package com.kchadaj.timer

import addPadding
import android.animation.AnimatorSet
import android.animation.ArgbEvaluator
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_timer.*
import android.animation.ValueAnimator
import color
import com.kchadaj.timer.views.CircularSeekBar
import getDimen
import isSoftwareNavBarVisible

class TimerActivity : AppCompatActivity(), TimerViewContract {

    var timerPresenter = TimerPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_timer)

        setAppNameTextViewPadding()
        setFabPadding()

        fab.setOnClickListener {
            timerPresenter.onTimerButtonClick()
        }

        circularSeekBar.setOnProgressChangeListener(object : CircularSeekBar.OnProgressChangeListener {
            override fun onProgressIndexChanged(progressIndex: Int) {
                timerPresenter.onProgressIndexChanged(progressIndex)
            }
        })
    }

    override fun onPause() {
        super.onPause()

        timerPresenter.onViewPause()
    }

    override fun getProgressIndex() = circularSeekBar.progressIndex

    override fun updateProgress(progress: Float) {
        circularSeekBar.progress = progress
        circularSeekBar.invalidate()
    }

    override fun enableCircularSeekBar(enable: Boolean) {
        circularSeekBar.isEnabled = enable
    }

    override fun setTimerButtonDrawable(id: Int) {
        fab.setImageResource(id)
    }

    override fun setTimerTextView(time: String) {
        timerTextView.text = time
    }

    override fun runColorTransitionAnimations() {
        val colorPrimary = color(R.color.colorPrimary)
        val colorPrimaryDark = color(R.color.colorPrimaryDark)
        val primaryColorAnimationListener: (ValueAnimator) -> Unit = {
            activityMainLayout.setBackgroundColor(it.animatedValue as Int)
            circularSeekBar.innerColor = it.animatedValue as Int
            circularSeekBar.labelColor = it.animatedValue as Int
            circularSeekBar.invalidate()
        }
        val primaryColorAnimation = prepareAnimation(colorPrimary, colorPrimaryDark, primaryColorAnimationListener)

        val colorSeekBar = color(R.color.colorSeekBar)
        val colorSeekBarDark = color(R.color.colorSeekBarDark)
        val seekBarColorAnimationListener: (ValueAnimator) -> Unit = {
            circularSeekBar.seekBarColor = it.animatedValue as Int
            circularSeekBar.invalidate()
        }
        val seekBarColorAnimation = prepareAnimation(colorSeekBar, colorSeekBarDark, seekBarColorAnimationListener)

        val animatorSet = AnimatorSet()
        animatorSet.play(primaryColorAnimation).with(seekBarColorAnimation)
        animatorSet.start()
    }

    override fun runFlashAnimation() {
        val flashAnimationDuration = 75L

        val colorFlash = color(R.color.colorFlash)
        val colorPrimary = color(R.color.colorPrimary)
        val colorPrimaryDark = color(R.color.colorPrimaryDark)
        val colorSeekBar = color(R.color.colorSeekBar)
        val colorSeekBarDark = color(R.color.colorSeekBarDark)

        val primaryColorFlashListener: (ValueAnimator) -> Unit = {
            activityMainLayout.setBackgroundColor(it.animatedValue as Int)
            circularSeekBar.innerColor = it.animatedValue as Int
            circularSeekBar.labelColor = it.animatedValue as Int
            circularSeekBar.invalidate()
        }

        val seekBarFlashListener: (ValueAnimator) -> Unit = {
            circularSeekBar.seekBarColor = it.animatedValue as Int
            circularSeekBar.invalidate()
        }

        val primaryColorFlashStart = prepareAnimation(colorPrimaryDark, colorFlash, primaryColorFlashListener, flashAnimationDuration)
        val primaryColorFlashEnd = prepareAnimation(colorFlash, colorPrimary, primaryColorFlashListener, flashAnimationDuration)
        val seekBarFlashStart = prepareAnimation(colorSeekBarDark, colorFlash, seekBarFlashListener, flashAnimationDuration)
        val seekBarFlashEnd = prepareAnimation(colorFlash, colorSeekBar, seekBarFlashListener, flashAnimationDuration)

        val animatorSet = AnimatorSet()
        animatorSet.play(primaryColorFlashStart).with(seekBarFlashStart)
        animatorSet.play(primaryColorFlashStart).before(primaryColorFlashEnd)
        animatorSet.play(primaryColorFlashEnd).with(seekBarFlashEnd)
        animatorSet.start()
    }

    override fun revertView() {
        val colorPrimary = color(R.color.colorPrimary)
        val colorSeekBar = color(R.color.colorSeekBar)

        activityMainLayout.setBackgroundColor(colorPrimary)
        circularSeekBar.innerColor = colorPrimary
        circularSeekBar.labelColor = colorPrimary
        circularSeekBar.seekBarColor = colorSeekBar
        circularSeekBar.invalidate()
    }

    private fun prepareAnimation(fromColor: Int, toColor: Int, updateListener: (ValueAnimator) -> Unit, duration: Long = 300L): ValueAnimator {
        val animation = ValueAnimator.ofObject(ArgbEvaluator(), fromColor, toColor)
        animation.duration = duration
        animation.addUpdateListener(updateListener)
        return animation
    }

    private fun setAppNameTextViewPadding() {
        appNameTextView.addPadding(top = appNameTextView.paddingTop + resources.getDimen("status_bar_height"))
    }

    private fun setFabPadding() {
        if (resources.isSoftwareNavBarVisible()) {
            fabLayout.addPadding(bottom = fabLayout.paddingBottom + resources.getDimen("navigation_bar_height"))
        }
    }
}
