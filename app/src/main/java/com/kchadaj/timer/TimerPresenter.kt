package com.kchadaj.timer

import android.os.CountDownTimer
import com.kchadaj.timer.utils.Utils

class TimerPresenter(val view: TimerViewContract): TimerPresenterContract {

    lateinit var timer: CountDownTimer
    private var isTimerCounting = false

    override fun onViewPause() {
        if (::timer.isInitialized) {
            stopTimer()
        }
    }

    override fun onTimerButtonClick() {
        if (isTimerCounting) {
            stopTimer()
        } else {
            startTimer()
            view.runColorTransitionAnimations()
            view.setTimerButtonDrawable(R.drawable.ic_timer_off)
            isTimerCounting = true
        }
    }

    override fun onProgressIndexChanged(progressIndex: Int) {
        updateTimerTextView(progressIndex.toFloat())
        if (progressIndex != 0) {
            view.updateProgress(progressIndex.toFloat())
        }
    }

    private fun startTimer() {
        timer = object : CountDownTimer(view.getProgressIndex() * 1000L, 10) {

            override fun onTick(millisUntilFinished: Long) {
                val value = millisUntilFinished / 1_000f // [seconds]
                view.updateProgress(value)
                updateTimerTextView(value)
            }

            override fun onFinish() {
                val progress = view.getProgressIndex().toFloat()
                updateTimerTextView(progress)
                view.updateProgress(progress)
                view.enableCircularSeekBar(true)
                view.setTimerButtonDrawable(R.drawable.ic_timer_on)
                if (isTimerCounting) {
                    view.runFlashAnimation()
                }
                isTimerCounting = false
            }
        }

        view.enableCircularSeekBar(false)
        timer.start()
    }

    private fun stopTimer() {
        timer.cancel()
        isTimerCounting = false
        view.revertView()
        timer.onFinish()
    }

    private fun updateTimerTextView(seconds: Float) = view.setTimerTextView(Utils.formatTime(seconds))
}
