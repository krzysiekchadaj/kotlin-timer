package com.kchadaj.timer.utils

import android.graphics.drawable.ColorDrawable
import android.support.annotation.ColorRes
import android.support.test.espresso.matcher.BoundedMatcher
import android.support.test.internal.util.Checks
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.TextView
import com.kchadaj.timer.views.CircularSeekBar
import org.hamcrest.Description
import org.hamcrest.Matcher

object CustomMatchers {

    fun withBackgroundColor(@ColorRes color: Int): Matcher<View> {
        Checks.checkNotNull(color)
        return object : BoundedMatcher<View, View>(View::class.java) {
            public override fun matchesSafely(view: View): Boolean {
                return color == (view.background as ColorDrawable).color
            }

            override fun describeTo(description: Description) {
                description.appendText("with background color: ")
            }
        }
    }

    fun withTextColor(@ColorRes color: Int): Matcher<View> {
        Checks.checkNotNull(color)
        return object : BoundedMatcher<View, TextView>(TextView::class.java) {
            public override fun matchesSafely(textView: TextView): Boolean {
                val colorId = ContextCompat.getColor(textView.context, color)
                return colorId == textView.currentTextColor
            }

            override fun describeTo(description: Description) {
                description.appendText("with text color: ")
            }
        }
    }

    fun withColor(innerColor: Int,
            seekBarColor: Int,
            sliderColor: Int,
            sliderTraceColor: Int,
            labelColor: Int): Matcher<View> {
        Checks.checkNotNull(innerColor)
        Checks.checkNotNull(seekBarColor)
        Checks.checkNotNull(sliderColor)
        Checks.checkNotNull(sliderTraceColor)
        Checks.checkNotNull(labelColor)
        return object : BoundedMatcher<View, CircularSeekBar>(CircularSeekBar::class.java) {
            public override fun matchesSafely(csb: CircularSeekBar): Boolean {
                return (innerColor == csb.innerColor
                        && seekBarColor == csb.seekBarColor
                        && sliderColor == csb.sliderColor
                        && sliderTraceColor == csb.sliderTraceColor
                        && labelColor == csb.labelColor)
            }

            override fun describeTo(description: Description) {
                description.appendText("with CircularSeekBar color: ")
            }
        }
    }

    fun withProgress(progress: Int, maxProgress: Int): Matcher<View> {
        Checks.checkNotNull(progress)
        Checks.checkNotNull(maxProgress)
        return object : BoundedMatcher<View, CircularSeekBar>(CircularSeekBar::class.java) {
            public override fun matchesSafely(csb: CircularSeekBar): Boolean {
                return progress == csb.progress.toInt() && maxProgress == csb.maxProgress
            }

            override fun describeTo(description: Description) {
                description.appendText("with CircularSeekBar progress: ")
            }
        }
    }

    fun withOrientation(rotationAngle: Int): Matcher<View> {
        Checks.checkNotNull(rotationAngle)
        return object : BoundedMatcher<View, CircularSeekBar>(CircularSeekBar::class.java) {
            public override fun matchesSafely(csb: CircularSeekBar): Boolean {
                return rotationAngle == csb.rotationAngle.toInt()
            }

            override fun describeTo(description: Description) {
                description.appendText("with CircularSeekBar rotation angle: ")
            }
        }
    }
}
