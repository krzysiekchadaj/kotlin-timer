package com.kchadaj.timer

import android.content.res.Resources
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.kchadaj.timer.utils.CustomMatchers.withBackgroundColor
import com.kchadaj.timer.utils.CustomMatchers.withColor
import com.kchadaj.timer.utils.CustomMatchers.withOrientation
import com.kchadaj.timer.utils.CustomMatchers.withProgress
import com.kchadaj.timer.utils.CustomMatchers.withTextColor

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Rule
import org.junit.Before

@RunWith(AndroidJUnit4::class)
class TimerActivityInstrumentedTest {

    private lateinit var resources: Resources
    private lateinit var theme: Resources.Theme

    @get:Rule
    public var timerActivity: ActivityTestRule<TimerActivity> = ActivityTestRule(TimerActivity::class.java)

    @Before
    fun setup() {
        resources = timerActivity.activity.resources
        theme = timerActivity.activity.theme
    }

    @Test
    fun timerScreen_isDisplayed() {
        onView(withId(R.id.activityMainLayout))
                .check(matches(isDisplayed()))
                .check(matches(withBackgroundColor(
                        resources.getColor(R.color.colorPrimary, theme)
                )))

        onView(withId(R.id.circularSeekBar))
                .check(matches(isDisplayed()))
                .check(matches(withColor(
                        resources.getColor(R.color.colorPrimary, theme),
                        resources.getColor(R.color.colorSeekBar, theme),
                        resources.getColor(R.color.colorAccent, theme),
                        resources.getColor(R.color.colorAccentDark, theme),
                        resources.getColor(R.color.colorPrimary, theme)
                )))
                .check(matches(withProgress(
                        resources.getInteger(R.integer.circularSeekBar_progress),
                        resources.getInteger(R.integer.circularSeekBar_maxProgress)
                )))
                .check(matches(withOrientation(
                        resources.getInteger(R.integer.circularSeekBar_rotationAngle)
                )))

        onView(withId(R.id.fab))
                .check(matches(isDisplayed()))

        onView(withId(R.id.appNameTextView))
                .check(matches(isDisplayed()))
                .check(matches(withText(resources.getString(R.string.app_name))))
                .check(matches(withTextColor(R.color.colorText)))

        onView(withId(R.id.timerTextView))
                .check(matches(isDisplayed()))
                .check(matches(withText(resources.getString(R.string.default_timer_value))))
                .check(matches(withTextColor(R.color.colorText)))

        onView(withId(R.id.timerUnitsTextView))
                .check(matches(isDisplayed()))
                .check(matches(withText(resources.getString(R.string.default_timer_units))))
                .check(matches(withTextColor(R.color.colorText)))
    }

    @Test
    fun onCountdownFinished_shouldRevert() {
        onView(withId(R.id.fab)).perform(click())

        onView(withId(R.id.timerTextView))
                .check(matches(withText(
                        resources.getString(R.string.default_timer_value)
                )))

        onView(withId(R.id.activityMainLayout))
                .check(matches(isDisplayed()))
                .check(matches(withBackgroundColor(
                        resources.getColor(R.color.colorPrimary, theme)
                )))

        onView(withId(R.id.circularSeekBar))
                .check(matches(isDisplayed()))
                .check(matches(withColor(
                        resources.getColor(R.color.colorPrimary, theme),
                        resources.getColor(R.color.colorSeekBar, theme),
                        resources.getColor(R.color.colorAccent, theme),
                        resources.getColor(R.color.colorAccentDark, theme),
                        resources.getColor(R.color.colorPrimary, theme)
                )))
    }
}
